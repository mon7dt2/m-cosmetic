import React from 'react'

const RootLayout = ({ children }: { children: React.ReactNode }) => {
  return (
    <html lang="vi">
      <body style={{ background: 'white' }}>{children}</body>
    </html>
  )
}

export default RootLayout
