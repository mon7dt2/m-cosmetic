/** @type {import('next').NextConfig} */
const path = require('path')

const nextConfig = {
    optimizeFonts: true,
    webpack(config) {
        config.resolve.alias['@'] = path.join(__dirname, 'src')
        return config
    },
    images: {
        formats: ['image/webp'],
        remotePatterns: [
            {
                protocol: 'https',
                hostname: '**',
            },
        ],
    },experimental: {
        serverActions: true,
    },compiler: {
        removeConsole: process.env.NEXT_PUBLIC_NODE_ENV === 'production'
    }
}

module.exports = nextConfig
